package guru.springframework.RecipieProject.repositories;

import guru.springframework.RecipieProject.domain.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category,Long> {
}
