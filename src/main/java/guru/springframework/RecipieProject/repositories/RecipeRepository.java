package guru.springframework.RecipieProject.repositories;

import guru.springframework.RecipieProject.domain.Recipe;
import org.springframework.data.repository.CrudRepository;

public interface RecipeRepository extends CrudRepository<Recipe,Long> {
}
