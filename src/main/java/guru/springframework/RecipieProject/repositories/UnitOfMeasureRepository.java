package guru.springframework.RecipieProject.repositories;

import guru.springframework.RecipieProject.domain.UnitOfMeasure;
import org.springframework.data.repository.CrudRepository;

public interface UnitOfMeasureRepository extends CrudRepository<UnitOfMeasure,Long> {
}
