package guru.springframework.RecipieProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecipieProjectApplication {

	public static void main(String[] args)
	{
		SpringApplication.run(RecipieProjectApplication.class, args);
	}
}
