package guru.springframework.RecipieProject.domain;

import javax.persistence.*;

@Entity
public class Note
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne
    private  Recipe recipe;
    private String RecipeNotes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public String getRecipeNotes() {
        return RecipeNotes;
    }

    public void setRecipeNotes(String recipeNotes) {
        RecipeNotes = recipeNotes;
    }
}
